import 'package:flutter/material.dart';

import 'menu.dart';

class add extends StatelessWidget {
  const add({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bg.png'), fit: BoxFit.cover),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(height: 50),
              Center(
                child: Container(
                  height: 300,
                  width: 240,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/1.png'), fit: BoxFit.fill),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Your Item has been Added to Cart',
                style: TextStyle(fontSize: 18, color: Colors.white54),
              ),
              const SizedBox(height: 270),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => menu()));
                  },
                  child: Text('                    Ok                    '))
            ],
          ),
        ),
      ),
    );
  }
}
